#Katalogid#

##TSV_automaatne##

Automaatselt morfanalüüsitud TSV-failid. 

Algfailid võetud: 
*materjalid/Morf_anal_ja_kontrollitud_failid/morf-analyysid-kasutajasonastiku-ja-lausepiiridega.zip	3.26 MB	**2019‑02‑27** (Morf analüüsid lausepiiridega)*


##TSV_kasitsi##

Käsitsi morfanalüüsitud failid.

Algfailid võetud: 
*materjalid/Morf_anal_ja_kontrollitud_failid/KontrollitudFailid (**2018‑10‑31**)*.

Failidele lisati lausepiirid skriptiga *materjalid/VAKK/scripts/sentenceBoundaries.py* .


##pakitud##

Kataloogide  *TSV_automaatne* ja *TSV_kasitsi* sisu zip arhiividena.


