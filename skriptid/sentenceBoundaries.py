#/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import pathlib


#a loeme sisse faili lausepiiridega
#teeme massiivi, kus need lausepiirid asusid
#token nr token txt


#collect parish names
files = os.listdir('/Users/rabauti/svnkrpsoft/trunk/rabauti/vallaprotokoll_vakk/KontrollitudFailid/')


filenames = {}

for name in files:
	parish = name.strip()
	print (parish)
	files2 = os.listdir('/Users/rabauti/svnkrpsoft/trunk/rabauti/vallaprotokoll_vakk/KontrollitudFailid/'+parish)
	for name2 in files2:
		
		code = name2.replace('_ctl.tsv','')
		if not code == name2:
			print ("\t" +  name2)
			if not parish in filenames:
				filenames[parish] = []
			filenames[parish].append(code)




#print (filenames)






for parish in filenames.keys():
	
	for file in filenames[parish]:


		tsv_s_file = open("/Users/rabauti/svnkrpsoft/trunk/rabauti/vallaprotokoll_vakk/morf-analyysid-tsv-lausepiiridega/%s/%s.tsv" % (parish, file))
		tsv_manual_file = open("/Users/rabauti/svnkrpsoft/trunk/rabauti/vallaprotokoll_vakk/KontrollitudFailid/%s/%s_ctl.tsv" % (parish, file)) 

		
		pathlib.Path("/Users/rabauti/svnkrpsoft/trunk/rabauti/vallaprotokoll_vakk/KontrollitudLausepiiriga/%s/" % parish).mkdir(parents=True, exist_ok=True)
		tsv_output_file = open("/Users/rabauti/svnkrpsoft/trunk/rabauti/vallaprotokoll_vakk/KontrollitudLausepiiriga/%s/%s_ctl.tsv" % (parish, file), 'w') 

		sys.stderr.write ( parish + '\t' + file + '\n')
		
		tsv_s_file_txt = tsv_s_file.read() 
		
		
		newLines = {}
		wordcount = 0
		token = None
		for line in tsv_s_file_txt.split("\n"):
			arr = line.split("\t")
			if  arr[0].strip() == '<s>':
				if wordcount: 
					newLines[wordcount] = token
					#sys.stderr.write('\tuus lause\t' + str(wordcount) + '\t' + token +'\n')
			elif  wordcount and arr[0].strip() == '</s>':
				continue
			elif len(arr[0].strip()):
				wordcount+=1;
				token = arr[0].strip()
				#sys.stdout.write('\tluges\t' +str(wordcount) + '\t'+ token +'\n')
			#else:
			#	sys.stdout.write('\tei lugenud\t' +str(wordcount) + '|'.join(arr) +'\n')
			
		tsv_s_file.close()
		#sys.stderr.write ( str(newLines) + '\n')
		
		tsv_manual_file_txt = tsv_manual_file.read() 
		
		wordcount = 0
		
		#faili algusesse <s>
		words = {}
		tsv_output_file.write ("<s>\t\t\t\t\t")
		tsv_output_file.write ("\n")
		for line in tsv_manual_file_txt.split("\n"):
			if not len(line.strip()) :continue
			arr = line.split("\t")
			if len(arr[0].strip()):
				#print (arr[0].strip())
				wordcount+=1;
				words[wordcount] = arr[0].strip()
		
			
			
			if wordcount-1 in newLines:
				if newLines[wordcount-1]!=words[wordcount-1]:
					sys.stderr.write('\tLausepiir on nihkunud, sõned ei lange kokku.\n')
					sys.stderr.write('\t'+ str(wordcount-1) +'\t' +words[wordcount-1] + '|||'+ newLines[wordcount-1] + '\n')
				tsv_output_file.write ("</s>\t\t\t\t\t")
				tsv_output_file.write ("\n")
				tsv_output_file.write ("<s>\t\t\t\t\t")
				tsv_output_file.write ("\n")
				del newLines[wordcount-1]
			tsv_output_file.write (line + "\n")
			
		
		tsv_output_file.write ("</s>\t\t\t\t\t")
		tsv_output_file.write ("\n")
		tsv_manual_file.close()
		tsv_output_file.close()
		
		
exit()	