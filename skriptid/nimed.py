#!/usr/bin/python
#-*- coding: utf-8 -*-
from __future__ import print_function
import os
import sys
from estnltk import Text
from bs4 import BeautifulSoup
import re
from collections import defaultdict
import distance

#Sisendfailide kaust
xmldir=sys.argv[1]
#Väljundfailide kaust
namesdir=sys.argv[2]
if not os.path.exists(namesdir):
	os.mkdir(namesdir)
#Korjame kõigepealt nimeüksused kokku
#Sinna kogume iga protokolli tekstiobjektid
corpus={}
#Ning sinna tulevad nende tekstiobjektide sõned koos nimeüksuste märgendustega
words={}
#Nimede ja sõnade sagedused
freq=defaultdict(int)
#Funktsioonisõnade loend. Sinna kogutakse määr- ja sidesõnad
funcwords=[]
for subdir in os.listdir(xmldir):
	if not os.path.exists(os.path.join(namesdir, subdir)):
		os.mkdir(os.path.join(namesdir, subdir))
	for filename in os.listdir(os.path.join(xmldir, subdir)):
		with open(os.path.join(xmldir, subdir, filename), encoding="utf-8") as fin:
			filecontent=fin.read()
			#Võtame välja protokolli sisu
			content=BeautifulSoup(filecontent, "lxml")
			content=content.find("sisu").getText()
			
			key=os.path.join(subdir, filename)
			corpus[key]=Text(content)
			words[key]=corpus[key].tag_labels()['words']
			for word in words[key]:
				#Kuna mõnikord on nimeüksus suvalise sõnaga sassi läinud ning seda saab mõnes kohas sagedusi vaadates parandada, nimede seisukohast on aga suure algustähe säilitamine oluline,
				#siis määrame kõigile sagedusloendis olevatele sõnadele suure algustähe
				w=word['text'].capitalize()
				freq[w]+=1
				if word['analysis'][0]['partofspeech']=="D":
					funcwords.append(w)
#funcwords=list(set(funcwords))
#print (len(funcwords))
#print (funcwords[:20])
#Kuna skripti testjooksutamisel sai selgeks, et funktsioonsõnade ja teisenduskauguste järgi ei saa nimetuvastust siiski automaatselt eemaldada,
#siis sai skripti väljundit vaadatud ning sagedamini esinenud funktsioonsõnad käsitsi listi pandud.
removedWords=['Kõrd', 'Siisgi', 'Seejärele', 'Sis', 'Vara', 'Villa', 'Edesi', 'Kaup' 'Kool', 'Eit', 'Kuhi', 'All', 'Väär', 
	'Vääri', 'Ja', 'Seeni', 'Seen', 'Kis', "Kül", 'Otsus', 'Kohalk', 'Mes', 'Maa',
	'Pant', 'Mõistm', 'Veel', 'Eila', 'Kokko', 'Aga', 'Järel', 'Kos', 'Kaub', 'Juures', 'Peap', 'Rahu', 'Sülla',
	'Maja', 'Kui', 'Tõiselt', 'Sell', 'Kaupa', 'Karja', 'Kõrral', 'Vakka', 'Ärä', 'Ema',
	'Mitte', 'Vimati', 'Tulli', 'Seeperast', 'Regi', 'Lina', 'Jarel', 'Raha', 'Nendasama', 'Kinni', 'Ärra', 'Siski', 'Magasi', 'Jagu', 'Ne', 'Võlgo',
	'Vasta', 'Isa', 'Perast', 'Väärt', 'Loeti', 'Veidi', 'Kes', 'Kost', 'Kige', 'Vaka', 'Peris', 'Tarvis', 'Kõrda', 'Kord',
	'Esite', 'Kohtomees', 'Pääkohtomees', 'Kohtumees', 'Kohtu', 'Kohto', 'Kaibas', 'Pääkohtumees', 'Protokolli']
#Kuna kohakääne isikunime järel viitab ilmselt kohale, siis loome käänete jaoks eraldi loendi, mida saab hiljem kasutada.
localCases=['sg ill', 'sg in', 'sg el', 'sg all', 'sg ad', 'sg abl', 'pl ill', 'pl in', 'pl el', 'pl all', 'pl ad', 'pl abl']
#Sõnastik, milles hoitakse nimeüksuste lõpus või selle järel olevaid väiketähelisi sõnu.
lowerAfter=defaultdict(int)
#Funktsioon, mis otsib tekstikorpusest etteantud sõna  ning annab kõigile leitud sõnadele etteantud märgendi. Enamasti kasutatakse siin koodis valetuvastuste eemaldamiseks.
def modifyTag(w, tag):
	for i in words:
		for index, word in enumerate(words[i]):
			if w==word['text']:
				#Kui valetuvastusele järgnev märgend on I, omistatakse sellele eemaldatud nime märgend.
				if index != len(words[i])-1:
					if "I" in words[i][index+1]['label']:
						words[i][index+1]['label']=word['label']
				
				word['label']=tag
				
#Funktsioon nimeüksuse pikkuse kindlakstegemiseks
#Argumendid on protokoll ja indeks, mille peal funktsiooni parajasti välja kutsutakse.

def getLength(pr, ind):
	length=1
	while True:
		#print (pr[ind]['text'], " ", end='')
		#print (ind)
		if 'O' in pr[ind]['label']:
			#print (length)
			return length-1
		else:
			length+=1
			ind-=1

		
#Kontrollime enne ja pärast töötlust, kas asi ikka toimib. Seepärast vaatame, kui suur osa sõnedest on saanud etteantud märgendi enne ja pärast töötlust.
def check(tag):
	count=0
	for i in corpus:
		words=corpus[i]['words']
		for word in words:
			if word['label']==tag:
				count+=1
	return count
#print (check("O"))
bigrams=defaultdict(list)
trigrams=defaultdict(list)
#Skripti jooksutamisel sai selgeks, et need on sagedamini esinevad nimeüksusele järgnevad sõnad, mis suure tõenäosusega määravad nimeüksuse liigi.
locs=['talo', 'talu', 'küla', 'valla', 'linn', 'valda', 'vallavalitsus']
orgs=['kohus', 'kohtu']

for i in words:
	parish=i.split("/")[0]
	#print(parish)
	#Kuna words]i] on koodis pikapeale üsna ebamugav kirjutada, anname sellele väärtuseks tmp
	tmp=words[i]
	for index, j in enumerate(tmp):
		#Vaatame, kas tegu on nimeüksusega
		if j['label'] != "O":
			#Kuna mõnikord on ni	meüksustena tuvastatud asesõnu, siis viskame need nimede hulgast välja
			if j['analysis'][0]['partofspeech']=="P":
				#print (j['text'])
				removedWords.append(j['text'])
			
			"""
			#Võtame sagedasemad funktsioonisõnad välja
			#Kuna hiljem selgus, et automaatselt see eriti hästi ei toimi, siis praegu on see koodiosa välja kommenteeritud.
			for word in funcwords:
				if distance.levenshtein(j['text'], word) <=1:
					#print (j['text'], freq[j['text']], word)
					modifyTag(j['text'], "O")
					continue
			"""
			#Kuna järgmises osas vaatame ka järgmist sõna, siis on oluline, et ei oleks päris lõpus.
			if index==len(tmp)-1:
				continue
			#Võtame välja sagedasemad kooloniga lõppevad ühesõnalised nimeüksused
			#Kuna näiteks nimi Rattasep oli korpuses sagedusega 33 ning sõnad nagu mõistetu 38 ja suurema sagedusega, siis määrame sageduseks 35
			if "B" in j['label'] and tmp[index+1]['text']==":" and freq[j['text']] >= 35:
				#print (j['text'], freq[j['text']])
				removedWords.append(j['text'])
			
			#Vaatame nimeüksuste lõpus või selle järel olevaid väiketähelisi sõnu.
			if j['text'][0].islower() and tmp[index+1]['label']=="O" and ("LOC" in j['label'] or "ORG" in j['label']):
				lowerAfter[j['text']]+=1
			#elif tmp[index+1]['text'][0].islower() and tmp[index+1]['label']=="O" and tmp[index+1]['text'] in lowerAfter:
	#			lowerAfter[tmp[index+1]['text']]+=1
			
			"""
			#Kontrollime, kas oleme nimeüksuse lõpus, kas selle pikkus on suurem kui 3 tekstisõne ning kas viimane nendest on kohakäändes. Lisaks tuleks kontrollida, kas eelnev tekstisõne on kirjavahemärk.
			#Kui jah, siis ilmselt on tegemist ikkagi kohanimega.
			if tmp[index+1]['label']=='O' and getLength(tmp, index)>=3 and j['analysis'][0]['form'] in localCases and j['label']=="I-PER" and tmp[index-1]['analysis'][0]['partofspeech']=="Z":
				#print (j['text'], getLength(tmp, index))
				#j['label']='B_LOC'
		"""
		#Kogume kokku bigrammid ja trigrammid
		#Kuna kogu vajalik info on BIO märgendite kihi all kenasti olemas, siis on n-gramm lihtsalt tuple, milles hoitakse vastavat infot.
		#Lisaks vaatame ka mustreid, kus nimeüksusele võib järgneda koha või organisatsiooninimi
		
		#Vaatame, kas üldse õnnestub ühe võrra edasi vaadata.
		if index==len(tmp)-1:	
			continue
		if j['text'][0].isupper() and tmp[index+1]['text'][0].isupper():
			bigrams[parish].append((j, tmp[index+1]))
		#Vaatame koha- ja organisatsiooninimede mustreid.
		for loc in locs:
			if j['label']=="B-ORG" and tmp[index+1]['text'].startswith(loc):
				#print (j['text'], tmp[index+1]['text'])
				j['label']="B-LOC"
		for org in orgs:
			if j['label']=="B-LOC" and re.match(r'[a-z]+'+org+'', tmp[index+1]['text']):
				#print (j['text'], tmp[index+1]['text'])
				j['label']="B-ORG"
		#Vaatame, kas saab üldse kaks sammu edasi vaadata
		if index==len(tmp)-2:	
			continue
		if j['text'][0].isupper() and tmp[index+1]['text'][0].isupper() and tmp[index+2]['text'][0].isupper():
			trigrams[parish].append((j, tmp[index+1], tmp[index+2]))
		#Vaatame koha- ja organisatsiooninimede mustreid.
		for loc in locs:
			if j['label']=="B-LOC" and tmp[index+1]['label']=="I-LOC" and tmp[index+2]['text'].endswith(loc):
				#print (j['text'], tmp[index+1]['text'], tmp[index+2]['text'])
				j['label']="B-ORg"
				tmp[index+1]['label']="I-ORG"
				tmp[index+2]['label']="I-ORG"
		for org in orgs:
			if j['label']=="B-LOC" and tmp[index+1]['label'] == 'I-LOC' and re.match(r'[a-z]+'+org+'', tmp[index+2]['text']):
				#print (j['text'], tmp[index+1]['text'], tmp[index+2]['text'])
				j['label']="B-ORG"
				tmp[index+1]['label']="I-ORG"
				tmp[index+2]['label']="I-ORG"
		
removedWords=set(removedWords)
for word in removedWords:
	modifyTag(word, "O")
#print (check("O"))
#print (check("B-PER"))

#Funktsioon n-grammist teksti mugavamaks kättesaamiseks
def ngramText(ngram):
	text=""
	for i in ngram:
		text+=" "+i['text']
	return text.strip()
#Funktsioon n-grammist BIO märgendite info mugavaks kättesaamiseks
def ngramTag(ngram):
	tag=""
	for i in ngram:
		tag+=" "+i['label']
	return tag.strip()
"""
#Skripti testimisel vaadatakse, millised sõnad ja milliste sagedustega on nimeüksuse lõpus või selle järel. Seejärel vaadatakse väljundit ning lisatakse käsitsi vastavad sõnad kas siis koha- või organisatsiooninimede loendisse.
for i in lowerAfter:
	#print (i, lowerAfter[i])
"""
"""
for i in words:
	for index, word in enumerate(words[i]):
		#Kuna korpuses esines kohtadele viitavate sõnade loendis peamiselt selliseid vorme, mis kõik algavad loendis olevate sõnadega
		#nt küla, külas, külast, siis vaatame, kas see algab loendis oleva sõnaga.
		for loc in locs:
			if word['text'].startswith(loc):
				if word['label'] !="O":
					word['label'] = "O"
				tmpindex=index
				if getLength(words[i], index-1)>2:
					continue
				while True:
					tmpindex-=1
					#print (tmpindex)
					if words[i][tmpindex]['label'] == "B-ORG":
						words[i][tmpindex]['label'] = "B-LOC"
						#print (word['text'], words[i][tmpindex]['text'], words[i][tmpindex]['label'])
						break
					elif words[i][tmpindex]['label'] == "I-ORG":
						words[i][tmpindex]['label'] = "I-LOC"
						#print (word['text'], words[i][tmpindex]['text'], words[i][tmpindex]['label'])
					else:
						break
		
		#Vaatame organisatsiooninimesid. Kuna enamasti on kohtu või kohus just selle lõpus (nt kihelkonnakohus või sillakohus, siis uurime just lõppe).
		for org in orgs:
			if word['text'].endswith(org) and word['text']!=org:
				if word['label'] != 'I-ORG':
					word['label'] = "I-ORG"
				#print (word['text'])
				tmpindex=index
				while True:
					tmpindex-=1
					if words[i][tmpindex]['label'] == "B-LOC":
						words[i][tmpindex]['label'] = "B-ORG"
						#print (words[i][tmpindex]['text'], words[i][tmpindex]['label'])
						break
					elif words[i][tmpindex]['label'] == "I-LOC":
						words[i][tmpindex]['label'] = "I-ORG"
						#print (words[i][tmpindex]['text'], words[i][tmpindex]['label'])
					else:
						break
		"""
for parish in trigrams:
	for i in trigrams[parish]:
		for j in trigrams[parish]:
			#Vaatame, kas mõni isikunimi on jäänud märgendamata. Lisaks vaatame, et see ei sisaldaks initsiaali, nendega tegeleme hiljem.
			if ngramTag(i)=="B-PER I-PER I-PER" and "PER" not in ngramTag(j) and "." not in ngramText(i) and "." not in ngramText(i):
				if distance.levenshtein(ngramText(i), ngramText(j)) <= 2:
					#print (ngramText(i), ngramText(j))
					#print (ngramTag(i), ngramTag(j))
					j[0]['label']="B-PER"
					j[1]['label']="I-PER"
					j[2]['label']="I-PER"


for parish in bigrams:
	for i in bigrams[parish]:
		for j in bigrams[parish]:
			#Vaatame, kas mõni isikunimi on jäänud märgendamata.
			#print (ngramTag(i))
			
			
			if ngramTag(i)=="B-PER I-PER" and "PER" not in ngramTag(j):
				#print ("jah")
				#Vaatame, ega üks võrreldavatest ei sisalda punkti (initsiaalidega tegeleme hiljem)
				if "." not in ngramText(i) and "." not in ngramText(i) and distance.levenshtein(ngramText(i), ngramText(j)) <= 1:
					#print (ngramText(i), ngramText(j), ngramTag(j))
					j[0]['label']="B-PER"
					j[1]['label']="I-PER"
				#Vaatame initsiaale
				elif len(j[0]['text']) < 4:
					firstName=i[0]['text']
					surname1=i[1]['text']
					initial=j[0]['text']
					surname2=j[1]['text']
					#Vaatame, kas initsiaali ja eesnime algustähed klapivad ning võrdleme perenimede teisenduskaugusi
					if initial[0]==firstName[0] and distance.levenshtein(surname1, surname2)<=1:
						#print (ngramText(i), ngramText(j))
						j[0]['label']="B-PER"
						j[1]['label']="I-PER"
					
#print (check("O"))
#print (check("B-PER"))
#Kuna märgenduse parandamisel võisid alguse märgendid katki minna ning sellepärast hakkab märgend hoopis I-ga, teeme selle korda.
for i in words:
	for index, j in enumerate(words[i]):
		if index ==0:
			continue
		if "I" in j['label']:
			nametype=j['label'].split("-")[-1]
			if nametype not in words[i][index-1]['label']:
				newLabel="B-"+nametype
				#print (j['label'], words[i][-index-1]['label'], newLabel, nametype)
				j['label']=newLabel
				

#Kuna xml-märgenditega on mugavam kasutada named_entities kihti, siis konstrueerime selle.
for i in corpus:
	#print (i)
	with open(os.path.join(xmldir, i), encoding="utf-8") as fin:
		filecontent=fin.read()
		#Võtame välja protokolli sisu
		content=BeautifulSoup(filecontent, "lxml")
		content=content.find("sisu").getText()
		#Kuna indeksid lähevad märgendite lisamisel nihkesse, siis peame selle üle arvestust
		shift=0
		#print (i)
		tags=corpus[i].tag_named_entities()['named_entities']
		for x in tags:
			start=x['start']+shift
			end=x['end']+shift
			name=content[start:end]
			#print (name)
			if x['label']=="PER":
				tag="<isik>"+name+"</isik>"
			if x['label']=="LOC":
				tag="<koht>"+name+"</koht>"
			if x['label']=="ORG":
				tag="<org>"+name+"</org>"
			#Kuna regulaaravaldised läksid lõpuks keerukaks ja hakkasid tekitama probleeme, siis neid antud hetkel ei kasuta.
			#uusSisu=re.sub(r'[^>]'+nimi+'[^<]([:punct:]*)', r''+margend+'\1', uusSisu)
			#test=re.search(r'([^(>|a-z)])'+nimi+'([^(<|a-z)])', uusSisu).groups(0)
			#print (len(test))
			#uusSisu=re.sub(r'([^(>|a-z)])'+nimi+'([:punct:]| |$)', r'\1'+margend+'\3', uusSisu)
			#Asendame nime märgendatud nimega
			content=content[:start]+tag+content[end:]
			#Ning lisame nihkele juurde märgendite pikkuse
			shift+=len(tag)-len(name)
		#Paneme uue sisu sisu-märgendite vahele
		filecontent=re.sub(r'<sisu>[^<]*</sisu>', r'<sisu>'+content+'</sisu>', filecontent)
		with open(os.path.join(namesdir, i), "w") as fout:
			fout.write(filecontent)

