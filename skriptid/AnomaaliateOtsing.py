#!/usr/bin/python3
import os
import re
kohtumehed=[]
for subdir in os.listdir("xml"):
	if not os.path.isdir(os.path.join("xml", subdir)):
		continue
	for filename in os.listdir(os.path.join("xml", subdir)):
		with open(os.path.join("xml", subdir, filename)) as fin:
			out=""
			sisu=fin.read()
			nimed=re.findall("<nimi>(.*?)</nimi>", sisu)
			#print (filename)
			#print (nimed)
			if len(nimed)==0:
				out+="ei leidnud\n"
			else:
				for nimi in nimed:
					if nimi in kohtumehed:
						continue
					kohtumehed.append(nimi)
					out+=nimi+"\n"
			out=out.strip()
			if len(out) > 0:
				print (filename, out)
