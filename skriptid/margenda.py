#!/usr/bin/python3
import os
import re
import sys
from estnltk import Text
from bs4 import BeautifulSoup
#Sisendfailide kaust
html=sys.argv[1]
#Väljundfailide kaust
xml=sys.argv[2]
if not os.path.exists(xml):
	os.mkdir(xml)
#Loeme kõigepealt teemad sisse.
#Teemade dict. Võti on failinimi ning väärtus teema(d)
teemad={}
with open(os.path.join(html, "teemad.html")) as fin:
	sisu=fin.read()
	soup=BeautifulSoup(sisu, "html.parser")
	#Kuna iga teema on eraldi html failina, siis leiame kõigepealt viited nendele.
	lingid=soup.find_all("a")
	for link in lingid:
		#Kuna iga teema fail sisaldab märksõna list, siis otsime neid.
		if 'list' in link['href']:
			#Teema lingi <a> märgendite vahelt
			teema=link.get_text()
			#Võtame numbri eest ära ja asendame reavahe tühikuga.
			teema=re.sub("[0-9]+\. ", "", teema)
			teema=teema.replace("\n", " ")
			teema=re.sub(" +", " ", teema)
			#Ja xml-stiilis märgendid juurde
			teema="<teema>"+teema+"</teema>\n"
			#Vaatame, mis protokolle teema fail sisaldab.
			with open(os.path.join(html, link['href'])) as fin2:
				sisu2=fin2.read()
				sisu2=BeautifulSoup(sisu2, 'html.parser')			
				lingid2=sisu2.find_all("li")
				for link2 in lingid2:
					#Vaatame, mis failile viidatakse.
					link2=link2.find("a")['href']
					#Jätame ainult failinime
					fail=link2.split("/")[1]
					#Kui sellise võtmega kirje juba eksisteerib, liidame järgmise teema sinna otsa.
					#Kui ei, anname teema kirje väärtuseks.
					if fail in teemad and teemad[fail]!=teema:
						teemad[fail]=teemad[fail]+teema
					else:
						teemad[fail]=teema


for subdir in os.listdir(html):
	if not os.path.isdir(os.path.join(html, subdir)):
		continue
	for filename in os.listdir(os.path.join(html, subdir)):
		#Jätame index.html ja list.html failid välja
		if filename=="index.html" or filename=="list.html":
			continue
		print (filename)
		with open(os.path.join(html, subdir, filename), encoding="utf-8") as fin:
			failisisu=fin.read()
			#Kuna mõnikord on </pre> märgend kusagil mujal või puudub hoopis, siis tuleb midagi muud teksti lõpuks võtta.
			#Praegu tundub, et selleks sobib kõige paremini <br>
			try:
				failisisu=re.findall("<pre>(.*?)</pre>", failisisu, re.DOTALL)[0]
			except IndexError:
				failisisu=re.findall("<pre>(.*?)<br>", failisisu, re.DOTALL)[0]
			#Ja hakkame teksti reakaupa vaatama.
			#failisisu=failisisu.strip()
			read=failisisu.split("\n")
			failisisu=failisisu.strip()
			index=0
			"""
			#Rea indeks, millest alates tekstiosi otsima hakatakse. Kuna enamasti on esimene rida tühi, alustame teisest.
			if read[1]=="":
				index=0
			else:
				index=1
			"""
			
			#Funktsioon, mille abil tekstiosad leitakse
			#Kui jõuab tühja reani, tagastab leitud osa ja jätab indeksi meelde, et järgmisi otsida.
			
			def findParts():
				global index
				global read
				result=[]
				#Muutuja näitamaks, kas tühi rida on juba olnud. Kui jah ning järgmine rida sisaldab teksti, siis tagastame eelnenud osa.
				tyhi=False
				for k in range(index, len(read)-1):
					#Kuna meie mõistes tühi rida võib sisaldada siiski tühikuid, tuleb kontrollida, ega rida sõnu ei sisalda.
					#Lisaks võivad osad olla aeg-ajalt eraldatud ka kahe tühja reaga, seega tuleb vaadata, ega ei olda tekstiosa alguses.
					if not re.match(".*\w.*", read[k]) and k > index:
						tyhi=True
					if re.match(".*\w.*", read[k]):
						if tyhi==True:
							index=k
							break
						else:
							result.append(read[k])
				return result

			#Vaatame, kas valla alamkaust xml kaustas eksisteerib. Kui mitte, siis loome selle.
			if not os.path.exists(os.path.join(xml, subdir)):
				os.mkdir(os.path.join(xml, subdir))
			uus=filename[:-4]+"xml"
			with open(os.path.join(xml, subdir, uus), "w", encoding="utf8") as fout:
				fout.write("<protokoll>\n")
				fout.write("<head>\n")
				pealkiri=" ".join(findParts())
				#Mõnikord võib kuupäev olla pealkirja asemel. Kui pealkiri tähemärke ei sisalda, on tõenäoliselt tegemist kuupäevaga.
				aeg=[]
				#Vaatame, kas pealkiri sisaldab piisavalt tähemärke. Mõnikord võib kuupäevas ka üksikuid leiduda, seepärast määrame miinimumi.
				if [c.isalpha() for c in pealkiri].count(True)< 5:
					aeg=[pealkiri]
					pealkiri=" ".join(findParts())
				#Mitmekordsed tühikud välja
				pealkiri=re.sub(" +", " ", pealkiri)
				pealkiri=pealkiri.strip()
				pealkiri=pealkiri.strip("'")
				#Enamasti on kuupäev pealkirjast tühja reaga eraldatud, mõnikord aga mitte, nii et see võib olla pealkirja külge kleepunud peale </b> märgendit.
				if not pealkiri.endswith("</b>"):
					aeg.append(pealkiri[pealkiri.find("</b>")+4:])
					aeg[0]=aeg[0].strip()
					#print (pealkiri)
					pealkiri=pealkiri[:pealkiri.find("</b>")+4:]
				
				if not pealkiri.startswith("<b>"):
					pealkiri="<b>"+pealkiri
				if not pealkiri.endswith("</b>"):
					pealkiri=pealkiri+"</b>"
				pealkiri=pealkiri.replace("b>", "pealkiri>")
				#print (pealkiri)
				#Enamasti on kuupäev pealkirjast tühja reaga eraldatud, mõnikord aga mitte, nii et see võib olla pealkirja külge kleepunud peale </pealkiri> märgendit.
				if not pealkiri.endswith("</pealkiri>"):
					aeg.append(pealkiri[pealkiri.find("</pealkiri>")+11:])
					aeg[0]=aeg[0].strip()
					pealkiri=pealkiri[:pealkiri.find("</pealkiri>")+11:]
				
				fout.write(pealkiri+"\n")
				try:
					fout.write(teemad[filename])
				except:
					pass
				vald="<vald>"+subdir.capitalize()+"</vald>\n"
				vald=vald.replace("-", " ")
				fout.write(vald)
				#print (aeg)
				if len(aeg)==0 or len(aeg[0])==0:
					aeg=findParts()
				#print (aeg[0])
				#Kui aeg sisaldab mingit numbrit, on kõik korras. Kui ei, siis on see osa ilmselt puudu ning ilmselt on tegemist hoopis kohtumeestega.
				if re.match("[0-9]", aeg[0]):
					aeg="".join(aeg)
					number=""
					if ":" in aeg:
						number=aeg[aeg.find(":")+1:]
						aeg=aeg[:aeg.find(":")]
					#print (number)
					
					#Kuna aeg võib olla kirjutatud erineval kujul, teeme selle igal pool üheseks.
					aeg=aeg.replace(" ", ".")
					aeg=aeg.replace("..", ".")
					aeg=aeg.split(".")
					for x in aeg:
						x=x.strip()
					#print (aeg)
					aeg[0]=aeg[0].zfill(2)
					aeg[1]=aeg[1].zfill(2)
					if len(aeg[1])==4:
						aeg.append(aeg[1])
						aeg[1]="xx"
					#Mõnikord on kuupäeva lõpus ka mingit sodi. Samas peame arvestama, et kirjas võib olla ka vahemik. Seepärast vaatame, kas kolmas arv on aastaarvu moodi.
					try:
						#print (aeg[2])
						if re.match('\\d\\d\\d', aeg[2]):
							aeg=aeg[:3]
					except IndexError:
						pass
					#Kui aastaarv pole neljakohaline, asendame lõpus olevad x-ga. Kui aastat pole märgitud, määrame selleks xxxx.
					if len(aeg)>=3:
						multiplier=4-len(aeg[-1])
						aeg[-1]=aeg[-1]+(multiplier*"x")
					else:
						aeg.append("xxxx")
					#aeg=aeg[:3]
					aeg=".".join(aeg)
					fout.write("<aeg>"+aeg+"</aeg>\n")
					fout.write("<number>"+number+"</number>\n")
					kohtumehed=findParts()
				#Aeg ikkagi numbrit ei sisalda ning ilmselt on selle asemel kohtumehed
				else:
					kohtumehed=aeg
					aeg=[]
				#Mõnikord on kuupäev peale kohtumehi. Seepärast kordame koodi.
				if len(aeg)==0 or len(aeg[0])==0:
					aeg=findParts()
					#print (aeg[0])
					#Kui aeg sisaldab mingit numbrit, on kõik korras. Kui ei, siis on see osa ilmselt puudu ning ilmselt on tegemist hoopis kohtumeestega.
					if re.match("[0-9]", aeg[0]):
						aeg="".join(aeg)
						number=""
						if ":" in aeg:
							number=aeg[aeg.find(":")+1:]
							aeg=aeg[:aeg.find(":")]
						#print (number)
						
						#print (aeg)
						#Kuna aeg võib olla kirjutatud erineval kujul, teeme selle igal pool üheseks.
						#print (aeg)
						aeg=aeg.replace(" ", ".")
						aeg=aeg.replace("..", ".")
						aeg=aeg.split(".")
						for x in aeg:
							x=x.strip()
						#print (aeg)
						aeg[0]=aeg[0].zfill(2)
						aeg[1]=aeg[1].zfill(2)
						#Kui aastaarv pole neljakohaline, asendame lõpus olevad x-ga. Kui aastat pole märgitud, määrame selleks xxxx.
						if len(aeg) >=3 :
							multiplier=4-len(aeg[-1])
							aeg[-1]=aeg[-1]+(multiplier*"x")
							#print (aeg)
						else:
							aeg.append("xxxx")
						#aeg=aeg[:3]
						aeg=".".join(aeg)
						fout.write("<aeg>"+aeg+"</aeg>\n")
						fout.write("<number>"+number+"</number>\n")
				
				#Protokolli ülejäänud sisu osa
				read=read[index:]
				#Kui sisu esimene või viimane rida on tühi, eemaldame selle:
				for id in range(-1, 1):
					if (read[id]=="" or "</pre>" in read[id]) and len(read)>1:
						read.pop(id)
				fout.write("<kohtumehed>\n")
				
				#Võrdleme kohtumehi ning sisu osa.
				#Kui kohtumeeste pikkus on suurem sisust ning esimene rida pikem kui sisus, siis on ilmselt kohtumehed ja sisu vahetusse läinud.
				#print (len(kohtumehed[0]), len(read[0]))
				#print (len(kohtumehed), len(read))
				if len(kohtumehed) >= len(read) and len(kohtumehed[0]) > len(read[0]):
					tmp=kohtumehed
					kohtumehed=read
					read=tmp
				
				#Kui kohtumeeste osa on sama, mis sisu, siis tõenäoliselt ei ole kohtumeeste nimed muust tekstist tühjade ridadega eraldatud.
				if kohtumehed[0] == read[0]:
					#Teeme kohtumeeste osa tühjaks
					kohtumehed=[]
					#ning eemaldame märksõna kohtumees või kohtomees sisaldava rea sisu osast ning lisame kohtumeeste listi.
					#Kuna mõnikord on kohtumehed ka teksti lõpus, siis peame seda tegema kaks korda: Seepärast loome selleks funktsiooni:
					
					def kohtumehedSisust(x):
						for id, rida in enumerate(x):
							#Kuna kohtumees on mõnikord lühendatud kui kohtom. või kohtum. ning mõnikord kasutatakse ka kohtumehe, siis kasutasin lühendatud regulaaravaldist.
							if re.match(".*(K|k)oht(u|o)m.*", rida):
								kohtumehed.append(rida)
							else:
								x=x[id:]
								break
						return x
					
					read=kohtumehedSisust(read)
					#Kui kohtumeeste pikkus on 0, siis on need ilmselt teksti lõpus. Pöörame listi ümber ning jooksutame funktsiooni uuesti.
					if len(kohtumehed)==0:
						#Pöörame tagurpidi ja vaatame tagant poolt.
						read=list(reversed(read))
						#print (read[0])
						read=kohtumehedSisust(read)
						#Ning pöörame õiget pidi tagasi.
						read=list(reversed(read))
				#Paneme kohtumehed üheks stringiks, et siis komadega eraldatud kohtumehed eraldi üksusteks seada.
				tmp="\n".join(kohtumehed)
				tmp=re.sub("(, )|(; )|( ja )|( ning )", "\n", tmp)
				kohtumehed=tmp.split("\n")
				#Hakkame rida haaval vaatama. Eraldame nime suurte algustähtede järgi.
				for mees in kohtumehed:
					#Lõpust igaks juhuks tühikud maha
					mees=mees.strip()
					mees=re.search(r"(([A-Z]|[Õ-Ü]{1})[a-ü]*\.? ([A-Z]|[Õ-Ü]{1})[a-ü]+)(\.?\)?$| [a-ü])", mees)
					if mees:
						nimi=''.join(mees.groups(0)[:1])
						#print (nimi)
						fout.write("<nimi>"+nimi+"</nimi>\n")
				
				fout.write("</kohtumehed>\n")
				fout.write("</head>\n")
				#print (read[0])
				fout.write("<sisu>\n")
				
				pikkus=len(read)
				for id, rida in enumerate(read):
					#Kontrollime, kas vaadeldud rida on tühi.
					#Kuna meie mõistes tühi rida võib sisaldada siiski tühikuid, tuleb kontrollida, ega rida tähti ei sisalda.
					#Lisaks tuleb kontrollida, ega rida pole viimane, kuna sageli on see tühi.
					#Kui jah, kirjutame järgmise rea algusse uue rea märgi.
					if not re.match(".*[a-z+].*", rida) and id < pikkus-1:
						read[id]="\n"+rida
					elif re.match("^[0-9+]\.", rida):
						read[id]="\n"+rida
					elif "\t" in rida:
						read[id]=rida+"\n"
				sisu=" ".join(read)
				#Kuna read sai tühikutega kokku pandud ning sageli on rea lõpus ka tühik, tuleb neid sageli kaks korda järjest.
				#Seepärast asendame topelttühikud ühega.
				sisu=sisu.replace("  ", " ")
				#Kuna tühja rea puhul sai pandud reavahetus ning read said tühikutega kokku pandud, siis on rea lõpus ja või  järgmise alguses tühik.
				sisu=re.sub(" ?\n ?", "\n", sisu)
				sisu=sisu.replace("\n\n", "\n")
				
				laused=Text(sisu).sentence_texts
				for lause in laused:
					fout.write(lause+"\n")
					
				fout.write("</sisu>\n")
				fout.write("</protokoll>")
				