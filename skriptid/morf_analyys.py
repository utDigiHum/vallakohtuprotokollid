#!/usr/bin/python3.5
# -*- coding: utf-8 -*-
#   Teostab vallakohtu protokollide morf analüüsi, salvestab tulemused 
#   TSV (tab separated values) failidena ning ühtlasi korjab ja väljastab 
#   analüüsi tulemuste statistika.
#     Python:   3.5.4
#     EstNLTK:  1.4.1
#
from __future__ import unicode_literals, print_function, absolute_import

from estnltk.text import Text

from bs4 import BeautifulSoup
import re
import csv
import sys, os, os.path
import json
import argparse
from estnltk.vabamorf.morf import disambiguate
# Kas morf analüüsi (TSV) väljundisse tuleks lisada puuduvad punktuatsiooni morf analüüsid?
lisa_punktuatsiooni_analyysid = True

# Kas statistikast tuleks lahutada punktuatsiooni analüüsid?
lahuta_punktuatsiooni_analyysid = True

# Kas TSV väljundfailidele lisatakse päis?
lisa_tsv_faili_p2is = False
#Kas analüüsimisel kasutatakse kasutajasõnastikku
kasuta_kasutajasonastikku=True

# Kas morfi lisatakse ka automaatselt leitud lausepiirid (märgendid <s> ja </s>)?
lisa_lausepiirid = False
#Kas tekst ühestatakse hiljem?
yhesta=True

# Leiab, mitu % moodustab c a-st ning vormistab tulemused sõne kujul;
def get_percentage_of_all_str( c, a ):
    return '{} / {} ({:.2f}%)'.format(c, a, (c*100.0)/a)

# Lisab Text objektile punktuatsiooni analüüsi 
# (kuna vaikimisi jäetakse ilma olemiseta ka punktuatsioon analüüsimata)
def add_punctuation_analysis ( text ):
    for word in text['words']:
        if len(word['analysis']) == 0:
            # Teeme kindlaks, kas tegemist on punktuatsiooniga
            if len(word['text']) > 0 and not any([c.isalnum() for c in word['text']]):
                # On punktuatsioon: genereerime oletamisega analüüsid ja lisame
                analysis = \
                    Text(word['text'], guess=True, disambiguate=True, propername=True ).analysis[0]
                # Kui mingil põhjusel peaks analüüs olema mitmene, 
                # jätame alles vaid esimese:
                if len(analysis) > 1:
                    analysis = [ analysis[0] ]
                word['analysis'] = analysis

def check_user_dictionary(text, parish):
    for word in text['words']:
        # Kas sõna on kasutajasõnastikus? 
        if word['text'] in user_dict[parish]:
            #print (word['text'])
            # Kirjutame analüüsi üle kasutajasõnastiku analüüsiga
            word['analysis'] = user_dict[parish][ word['text'] ]
            
            # Hea on panna ka sõnale märge juurde, et see pärineb
            # kasutajasõnastikust (juhuks, kui hiljem on vaja 
            # eristada tavalisi kirjakeele sõnu kasutajasõnastiku
            # sõnadest)
            word['from_user_dict'] = True
        elif word['text'] in user_dict['global']:
            # Kirjutame analüüsi üle kasutajasõnastiku analüüsiga
            word['analysis'] = user_dict['global'][ word['text'] ]
            
            # Hea on panna ka sõnale märge juurde, et see pärineb
            # kasutajasõnastikust (juhuks, kui hiljem on vaja 
            # eristada tavalisi kirjakeele sõnu kasutajasõnastiku
            # sõnadest)
            word['from_user_dict'] = True



# Kirjutab analüüsid TSV (tab-separated-values) failina
def write_analysis_tsv_file( text, out_file_name ):
    global lisa_tsv_faili_p2is
    global lisa_lausepiirid
    # Leiame lausepiirid (kokkuleppeliselt: reavahetused)
    sentence_boundaries = None
    if lisa_lausepiirid:
        sentence_boundaries = find_sentence_boundaries(text.text)
        assert len(sentence_boundaries) > 0
    with open(out_file_name, 'w', encoding='utf-8', newline='\n') as csvfile:
        fieldnames = ['word', 'root', 'ending', 'clitic', 'partofspeech', 'form']
        writer = csv.DictWriter(csvfile, delimiter='\t', fieldnames=fieldnames)
        if lisa_tsv_faili_p2is:
            writer.writeheader() # Lisame algusesse p2ise
        sentence_id = 0
        word_count  = 0
        for (word, analyses, (word_start, word_end)) in zip(text.word_texts, text.analysis, text.word_spans):
            # Lause algus
            if lisa_lausepiirid and sentence_boundaries != None and len(sentence_boundaries) > 0:
                 sentence_span = sentence_boundaries[sentence_id]
                 # Lause algus
                 if word_start == sentence_span[0]:
                    analysis_item = {}
                    analysis_item['word']='<s>'
                    analysis_item['root']=''
                    analysis_item['ending']=''
                    analysis_item['clitic']=''
                    analysis_item['partofspeech']=''
                    analysis_item['form']=''
                    writer.writerow(analysis_item)
            # Väljastame sõna analüüsid (kui neid leidus)
            for aid, analysis in enumerate(analyses):
                analysis_item = {}
                analysis_item['word']=word
                analysis_item['root']=analysis['root']
                analysis_item['ending']=analysis['ending']
                analysis_item['clitic']=analysis['clitic']
                analysis_item['partofspeech']=analysis['partofspeech']
                analysis_item['form']=analysis['form']
                if aid > 0:
                    analysis_item['word']=' '*len(word)
                writer.writerow(analysis_item)
            # Kui analüüse polnudki, väljastame tühja rea
            if len(analyses) == 0:
                analysis_item = {}
                analysis_item['word']=word
                analysis_item['root']='####'
                analysis_item['ending']=''
                analysis_item['clitic']=''
                analysis_item['partofspeech']=''
                analysis_item['form']=''
                writer.writerow(analysis_item)
            # Lause lõpp
            if lisa_lausepiirid and sentence_boundaries != None and len(sentence_boundaries) > 0:
                sentence_span = sentence_boundaries[sentence_id]
                if word_end == sentence_span[1]:
                   analysis_item = {}
                   analysis_item['word']='</s>'
                   analysis_item['root']=''
                   analysis_item['ending']=''
                   analysis_item['clitic']=''
                   analysis_item['partofspeech']=''
                   analysis_item['form']=''
                   writer.writerow(analysis_item)
                   sentence_id += 1
        if lisa_lausepiirid and sentence_boundaries != None and len(sentence_boundaries) > 0:
            assert sentence_id == len(sentence_boundaries), \
               '(!) Midagi l2ks lausepiiride panemisel viltu failis '+\
               str(out_file_name)+'; Pandi '+str(sentence_id)+' lausepiiri / '+\
               'tegelikult oli '+str(len(sentence_boundaries))+' lausepiiri;'



# Leiab sisendteksti (sõne) lausepiirid
# Kokkuleppeliselt: laused on reavahetusega 
# eraldatud
def find_sentence_boundaries( text ):
    assert isinstance(text, str) 
    results = []
    start = 0
    for char_id, char in enumerate(text):
        if char == '\n':
           end = char_id
           # Erand: mõnikord on lause alguses miskipärast tabulaatorid
           # Parandus: nihutame lause algust nii, et see poleks tabulaator
           while start < end:
               start_char = text[start]
               if not start_char.isspace():
                   break
               start += 1
           # Erand: mõnikord on lausepiir miskipärast tabulaatori järel
           # Parandus: nihutame lause lõppu nii, et lõpus oleks sõna, mitte tabulaator
           while start < end-1:
               end_char = text[end-1]
               if not end_char.isspace():
                   break
               end -= 1
           if start < end:
               results.append( (start, end) )
           start = end + 1
    return results



# Töötleb ühe XML kausta sisu:
#  1. Teostab protokollide morf analüüsid (oletamiseta);
#  2. Teostab järelparandused (punktuatsioonile analüüsid tagasi);
#  3. Kirjutab iga protokolli morf analüüsi eraldi faili;
#  4. Kogub kokku statistika tundmatute sõnade kohta;
#     Tagastab statiatika ennikuna:
#     (xml_files, analysed, unamb, unk_title, unk_punct, punct, total)
def process_xml_folder( root, files, out_dir ):
    global lisa_punktuatsiooni_analyysid
    xml_files = 0
    analysed  = 0
    unamb     = 0
    total     = 0
    unk_title = 0
    unk_punct = 0
    punct     = 0
    parish=root.strip("/")
    parish=parish.split("/")[-1]
    for fle in sorted(files):
        if not fle.endswith('.xml'):
            # Analüüsime ainult XML laiendiga faile
            continue
        xml_files += 1
        with open(os.path.join(root, fle), 'r', encoding="utf-8") as fin:
            failisisu=fin.read()
        # Võtame välja protokolli sisu
        sisu=BeautifulSoup(failisisu, "lxml")
        sisu=sisu.find("sisu").getText()
        # Teostame analüüsi ilma ühestamiseta
        text = \
            Text(sisu, guess=False, disambiguate=False, propername=False )
        text.tag_analysis()
        # Teostame järelparandused
        if lisa_punktuatsiooni_analyysid:
            add_punctuation_analysis( text )
        if kasuta_kasutajasonastikku:
            check_user_dictionary(text, parish)
        if yhesta:
            #Jätame enne automaatset ühestamist alles ainult "nud" ja "tud" partitsiibid.
            for word in text['words']:
                if len(word['analysis']) > 1:
                    for analysis in word['analysis']:
                        if analysis['form']=='tud' or analysis['form']=='nud':
                            word['analysis']=[analysis]
            sentences = text.divide('words', 'sentences')
            for sentence in sentences:
                # 1) rakendame lause sõnade järjendil morf ühestamist
                disambiguated = disambiguate(sentence)
                # 2) viime muutused sisse:
                #    asendame algsed analüüsid uute, ühestatud analüüsidega
                for orig, new in zip(sentence, disambiguated):
                    orig['analysis'] = new['analysis']
            #Mõned analüüsid jäävad siiski mitmeseks. Ühestame kirvemeetodil.
            for word in text['words']:
                plural=False
                if len(word['analysis']) > 1:
                    for analysis in word['analysis']:
                        if analysis['form'].startswith("pl"):
                            plural=True
                    for analysis in word['analysis']:
                        if analysis['form'].startswith("sg") and plural:
                            word['analysis']=[analysis]
        if kasuta_kasutajasonastikku:
            for word in text['words']:
                if word['analysis'][0]['lemma'].startswith("###"):
                    word['analysis']=[]
        # Kogume kokku statistika
        for word, analyses in zip(text.word_texts, text.analysis):
            is_punct = len(word) > 0 and not any([c.isalnum() for c in word])
            if len(analyses) > 0:
                analysed += 1
            if len(analyses) == 1:
                unamb += 1
            if len(analyses) == 0:
                # Jäädvustame tundmatu sõna tüübi
                if len(word) > 0:
                    if word[0].isupper():
                        unk_title += 1
                    if is_punct:
                        unk_punct += 1
            else:
                # Jäädvustame tavalise sõna tüübi
                if is_punct:
                    punct += 1
            total += 1
        # Kirjutame morf analüüsid TSV faili
        out_file_name = os.path.join(out_dir, fle.replace('.xml', '.tsv'))
        write_analysis_tsv_file( text, out_file_name )
    return xml_files, analysed, unamb, unk_title, unk_punct, punct, total


# >>> Siit algab programm
if len( sys.argv ) > 1:
    in_dir  = sys.argv[1]
    out_dir = sys.argv[2] if len( sys.argv ) > 2 else 'morph_analyses'
    user_dict_file=sys.argv[3] if len(sys.argv)>3 else "user_dict.json"
    if kasuta_kasutajasonastikku==True and os.path.exists(user_dict_file):
        with open(user_dict_file) as fin:
            user_dict=json.loads(fin.read())
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)
    faile  = 0
    kaustu = 0
    results_dict = {}
    if os.path.isdir( in_dir ):
        # Töötleme sisendkausta andmeid,
        #   salvestame tulemused ja kogume statistika
        for root, dirs, files in os.walk( in_dir ):
            (head, tail) = os.path.split(root)
            print( ' Töötlen kausta '+root+' ... ' )
            # Tekitame väljundkausta morfi jaoks
            local_output_dir = os.path.join(out_dir, tail)
            if out_dir != tail  and  in_dir != tail  and  \
               not os.path.exists(local_output_dir):
                os.makedirs( local_output_dir )
            if len(files) > 0:
                # Teostame ühe valla protokollide (XML failide) analüüsi
                xml_files, analysed, unamb, unk_title, unk_punct, punct, total = \
                    process_xml_folder( root, files, local_output_dir )
                # Agregeerime statistika
                if xml_files > 0:
                    faile += xml_files
                    # Korrigeerimised
                    if lahuta_punktuatsiooni_analyysid:
                        if lisa_punktuatsiooni_analyysid:
                            # Kui oletamine välja lülitada, siis jääb ka punktuatsioon 
                            # analüüsita. Lahutame selle sõnadest maha:
                            total -= punct
                            analysed -= punct
                            unamb -= punct
                            punct = 0
                        else:
                            # Kui oletamine välja lülitada, siis jääb ka punktuatsioon 
                            # analüüsita. Lahutame selle sõnadest maha:
                            total -= unk_punct
                            unk_punct = 0
                    percent_analysed = (analysed * 100.0) / total
                    results_tuple = (xml_files, analysed, unamb, unk_title, unk_punct, total, percent_analysed) 
                    results_dict[tail] = results_tuple
                    #break
            kaustu += 1
    print()
    print('  Faile: ', faile )
    print(' Kaustu: ', kaustu )
    print()
    
    # Sorteerime tulemused analüüsitute protsendi järgi;
    # Väljastame pingerea
    aggregated = [0, 0, 0, 0, 0, 0, 0]
    for key in sorted(results_dict, key = lambda x : results_dict[x][-1], reverse=True):
        (xml_files, analysed, unamb, unk_title, unk_punct, total, percent_analysed) = results_dict[key]
        print(' '+key+' (',xml_files,' XML faili )')
        print('    1. morf analüüsiga: '+get_percentage_of_all_str( analysed, total ))
        print('             sh ühesed: '+get_percentage_of_all_str( unamb, analysed ))
        print('    2. morf analüüsita: '+get_percentage_of_all_str( (total-analysed), total ))
        print('  sh suure algustähega: '+get_percentage_of_all_str( unk_title,(total-analysed) ))
        if not lahuta_punktuatsiooni_analyysid:
            print('      sh punkuatsioon: '+get_percentage_of_all_str( unk_punct,(total-analysed) ))
        print()
        for i in range(len(results_dict[key])):
            aggregated[i] += results_dict[key][i]
    print()
    print()
    # Väljastame koondtulemuse
    print(' Kogu korpuse koondtulemus: ')
    [xml_files, analysed, unamb, unk_title, unk_punct, total, percent_analysed] = aggregated
    print('    1. morf analüüsiga: '+get_percentage_of_all_str( analysed, total ))
    print('             sh ühesed: '+get_percentage_of_all_str( unamb, analysed ))
    print('    2. morf analüüsita: '+get_percentage_of_all_str( (total-analysed), total ))
    print('  sh suure algustähega: '+get_percentage_of_all_str( unk_title,(total-analysed) ))
    if not lahuta_punktuatsiooni_analyysid:
        print('      sh punkuatsioon: '+get_percentage_of_all_str( unk_punct,(total-analysed) ))
else:
    print(' Käivitamine: ')
    print('   '+sys.argv[0]+' [xml_sisendkaust] [xml_väljundkaust]?')