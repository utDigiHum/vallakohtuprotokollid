#!/usr/bin/python3
#autor: Gerth Jaanimäe
#Lahendusel kasutatud koodijuppi http://stackoverflow.com/questions/19970532/how-to-check-a-string-for-a-special-character
import re
import os
import sys
#html failide kaust
dir=sys.argv[1]
for i in os.listdir(dir):
	for j in os.listdir(os.path.join(dir, i)):
		with open(os.path.join(dir, i, j), encoding="utf-8") as fin:
			text=fin.read()
			
			for char in text:
				#Vaatame, kas märk on üks lubatutest.
				if not re.match("[a-zA-ZÕõÄäÖöÜüŠšŽž0-9_,.-?!()# \t\n-<>=/'\[\]\{\}½¼§¾|–…]", char):
					print (char, "\t", j)
