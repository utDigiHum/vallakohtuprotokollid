#!/usr/bin/python3
#Koostab morfoloogilise analüüsi kvaliteedi parendamiseks kasutajasõnastiku.
#Käivitamine kasutajasonastik.py <xml-nimeüksustega> <käsitsi-kontrollitud-failid> <tundmatute sõnade sagedused>
from estnltk import Text
import os
import re
import sys
from bs4 import BeautifulSoup
import json
import operator
import collections
import csv
#Nimeüksustega märgendatud xml failide kaust
xml=sys.argv[1] if len(sys.argv) >=2 else "xml-nimedega"
#Käsitsi kontrollitud failide kaust
checked=sys.argv[2] if len(sys.argv)>=3 else "KontrollitudFailid"
#Tundmatute sõnade sagedused
freqfile=sys.argv[3] if len(sys.argv)==4 else "tundmatute_sagedused.txt"
#Teisenduste fail
replacements_file=sys.argv[4] if len(sys.argv)==5 else "teisendused.srt"
unknown_words_file=sys.argv[5] if len(sys.argv)==6 else "tundmatud.srt"
#Vaatame, kas kasutajasõnastiku fail juba eksisteerib.
try:
	with open("user_dict.json") as fin:
		user_dict=json.load(fin.read())
except:
	user_dict=collections.defaultdict(dict)
	user_dict['global']={}


#Kõigepealt vaatame käsitsi kontrollitud failide pealt.
def getAnalysisFromFile(filename):
	global parish
	global user_dict
	lines=[]
	with open(filename) as fin:
		reader=csv.reader(fin, delimiter="\t")
		#Kuna csv reader objektis ei saa indeksitega toimetada, praegu on aga vaja vaadata eelmist elementi, tuleb read listi panna.
		
		for line in reader:
			lines.append(line)
	for index, line in enumerate(lines):
		try:
			#Kui analüüs on kontrollijale tundmatuks jäänud, jätame kõrvale.
			if re.match("¤", line[0]):
				continue
			#Kui sõna on morf analüsaatorile tundmatuks jäänud.
			if re.match(r'#[a-üA-Ü]+', line[1]):
				wordform=lines[index-1][0]
				analysis={}
				analysis['root']=line[1].strip("#")
				#Kui tegu on lühendiga, siis rida tsv failis enamasti täies pikkuses ei ole.
				#Lisaks on mõnikord reas tabid puudu, nii et seepärast tuleb just viimast elementi vaadata.
				if line[-1]=="Y":
					analysis['partofspeech']="Y"
					analysis['ending']=""
					analysis['form']=""
					analysis['clitic']=""
					user_dict[parish][wordform]=[analysis]
					continue
				analysis['ending']=line[2]
				analysis['clitic']=line[3]
				analysis['partofspeech']=line[4]
				analysis['form']=line[5] if len(line) ==6 else ""
				user_dict[parish][wordform]=[analysis]
		except IndexError:
			print ("Probleem rea töötlemisega: Tõenäoliselt on mingi tab puudu. Palun vaata järgmisest failist järgmist rida:")
			print (file)
			print (line)
			sys.exit(1)



if os.path.isdir(checked):
	for root, dirs, files in os.walk(checked):
		if len(files) > 0:
			parish=root.strip("/")
			parish=parish.split("/")[-1]
			for file in files:
				if "PaxHeader" in root:
					#print (root)
					continue
				getAnalysisFromFile(os.path.join(root, file))
with open(unknown_words_file) as fin:
	reader=csv.reader(fin, delimiter="\t")
	for line in reader:
		wordform=line[0]
		analysis={}
		analysis['root']=line[1]
		#Kui tegu on lühendiga, siis rida tsv failis enamasti täies pikkuses ei ole.
		#Lisaks on mõnikord reas tabid puudu, nii et seepärast tuleb just viimast elementi vaadata.
		if line[-1]=="Y":
			analysis['partofspeech']="Y"
			analysis['ending']=""
			analysis['form']=""
			analysis['clitic']=""
			user_dict[parish][wordform]=[analysis]
			continue
		analysis['ending']=line[2]
		analysis['clitic']=line[3]
		analysis['partofspeech']=line[4]
		analysis['form']=line[5] if len(line) ==6 else ""
		#print (wordform, analysis)
		user_dict['global'][wordform]=[analysis]

if os.path.exists(replacements_file):
	with open(replacements_file) as fin:
		for line in fin:
			line=line.split(" ")
			#Rea esimene element on algne sõna, teine see, mis morf analüsaatorile ette antakse.
			user_dict['global'][line[0]]=Text(line[1], guess=False, disambiguate=False, propername=False).analysis[0]

#Tegeleme pärisnimedega
#Funktsioon pärisnime lemma ja käände tuvastamiseks
def getAnalysis(names, word):
	analysis=""
	case_endings={'ta': 'ab',
		'lt': 'abl',
		'l': 'ad',
		'le': 'all',
		'st': 'el',
		'sse': 'ill',
		'ga': 'kom',
		't': 'p',
		}
	#Proovime lemmat kätte saada, selleks vaatame, milline sarnastest nimedest on kõige lühem. Sarnasuse aluseks võtame nime neli esimest tähte. Nimede pikkuste vahe ei tohiks olla üle kolme tähe.
	lemmaCandidates=[]
	for name in names:
		name=name.split(" ")
		for x in name:
			if word['text'][:4]==x[:4] and (len(x)-len(word['text']))<=3 and word['text'] not in user_dict:
				lemmaCandidates.append(x)
	if len(lemmaCandidates)>0:
		analysis=word['analysis'][0]
		analysis['lemma']=min(lemmaCandidates, key=len)
		analysis['root']=min(lemmaCandidates, key=len)
		analysis['form']=""
		if word['text']==analysis['lemma']:
			analysis['form']="sg n"
			return analysis
		for key in case_endings:
			if word['text'].endswith(key):
				analysis['form']='sg '+case_endings[key]
				analysis['ending']=key
		if analysis['form']=="":
			if len(word['text']) > len(analysis['lemma']):
				analysis['form']='sg g'
			else:
				analysis['form']='sg n'
	#print(word['text'])
	#print (analysis)
	return analysis



if os.path.isdir(xml):
	for root, dirs, files in os.walk(xml):
		if len(files) > 0:
			parish=root.strip("/")
			parish=parish.split("/")[-1]
			for file in files:
				names=[]
				with open(os.path.join(root, file)) as fin:
					content=BeautifulSoup(fin.read(), "lxml")
					content=content.find("sisu")
					for name in content.find_all(['isik', 'koht', 'org']):
						names.append(name.get_text())
				names=list(set(names))
				#Kõigepealt analüüsime nimesid ilma oletamiseta.
				#Kui neri käigus nimeüksuseks märgitu on morf analüüsi käigus juba nimena analüüsitud, siis jätame kõrvale. Lisaks viskame välja ka väikse algustähega sõnad.
				tmp=[]
				for name in names:
					text=Text(name, guess=False, propername=False, disambiguate=False)
					text.tag_analysis()
					for word in text.words:
						if not (len(word['analysis'])==1 and word['analysis'][0]['partofspeech']=='H') and word['text'][0].isupper():
							tmp.append(name)
							break
				names=tmp
				#Seejärel katsetame nimede puhul oletamist.
				for name in names:
					text=Text(name) #, guess=False, propername=False, disambiguate=False)
					text.tag_analysis()
					for word in text.words:
						
						analysis=getAnalysis(names, word)
						if not analysis=="":
							word['analysis']=[analysis]
						#Kui pärisnime ei ole vastava sõna kohta käivate sõnaliikide loendis, võtame analüüside seast esimese ning määrame sellele sõnaliigiks "H".
						#Kui on, siis valime välja selle analüüsi ja eemaldame ülejäänud.
						#Seejärel vaatame sama lühenditega.
						partsofspeech=[analysis['partofspeech'] for analysis in word['analysis']]
						if "H" in partsofspeech and word['text'] not in user_dict[parish]:
							index=partsofspeech.index("H")
							user_dict[parish][word['text']]=[word['analysis'][index]]
						elif 'Y' in partsofspeech:
							index=partsofspeech.index("Y")
							user_dict[parish][word['text']]=[word['analysis'][index]]
						else:
							word['analysis'][0]['partofspeech']='H'
							user_dict[parish][word['text']]=[word['analysis'][0]]

#Teeme sõnades mõned asendused
#Kõigepealt loeme sisse tundmatute sõnade sagedusfaili.
unknown_words={}
with open(freqfile) as fin:
	for line in fin:
		word=line.split("\t")[0]
		unknown_words[word]=line

def replaceLetters(old, new, parish):
	count=0
	for key in unknown_words:
		if (parish in unknown_words[key] or parish=="global") and key not in user_dict[parish]:
			new_word=re.sub(old, new, key)
			analysis=Text(new_word, guess=False, disambiguate=False, propername=False).analysis[0]
			if len(analysis) > 0:
				#print (analysis)
				#print (key, new_word)
				user_dict[parish][key]=analysis
				count+=1
	if count > 0:
		print (parish, old, new, count, "asendust")

def processParish(parish):
	replaceLetters("o$", "u", parish)
	replaceLetters("nd$", "nud", parish)
	replaceLetters("nu$", "nud", parish)
	replaceLetters("nod$", "nud", parish)
	replaceLetters("nuvad$", "sid", parish)
	replaceLetters("sivad$", "sid", parish)
	replaceLetters("matta$", "mata", parish)
	replaceLetters("va$", "vad", parish)

processParish('laiuse')
processParish('alatskivi')
processParish('vastse-nõo')
processParish('kiuma')
processParish('navesti')
processParish('tarvastu')
processParish('uue-suislepa')
processParish('mihkli')
processParish('maasi')
replaceLetters("ö", "õ", "maasi")
replaceLetters("gg", "g", "global")
replaceLetters("bb", "b", "global")
replaceLetters("dd", "d", "global")
replaceLetters("p$", "b", "global")
replaceLetters("^temma", "tema", "global")

#for key in user_dict['laiuse']:
#	print (key, user_dict['laiuse'][key])
#Ühestamiseks on vajalik mingisugunegi analüüs. Seepärast lisame ikka veel tundmatuks jäänud sõnadele pseudoanalüüsi.
for word in unknown_words:
	wordform=word.split("\t")[0]
	if wordform not in user_dict['global']:
		analysis=[{'clitic': '',
			'ending': '0',
			'form': 'sg n',
			'lemma': "###"+wordform,
			'partofspeech': 'S',
			'root': "###"+wordform,
			'root_tokens': ["###"+wordform]}]
		user_dict['global'][wordform]=analysis

json = json.dumps(user_dict) #, indent=4, separators=(':', ','))
with open ("user_dict.json", "w") as fout:
	fout.write(json)
