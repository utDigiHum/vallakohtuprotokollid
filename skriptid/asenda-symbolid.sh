#!/bin/bash
#Asendab html failides olevad erisümbolid
[[ "$PWD" =~ skriptid ]] && cd ..

#Kõigepealt kopeerib kõik html failid uude kausta.
test -d html-puhastatud || cp -r html-symbolitega html-puhastatud
#Ning seejärel teeme vajalikud asendused
#Võttes vanade html kaustast failid ja kirjutades tulemused uute kausta.
#cd html-symbolitega

#Selleks loome funktsiooni
function asenda {
sed "s/$2/$3/g" < html-symbolitega/$1 > html-puhastatud/$1
}

asenda karevere/kr0019.html \` \'
asenda kokora/ko0012.html \� \§
asenda navesti/na0098.html a\` à
asenda suure-konguta/sk0025.html ð š
asenda vaike-rongu/vr0044.html ’ \'
asenda vaike-rongu/vr0069.html ’ \'
asenda vastse-noo/vn0116.html \` \'
asenda vastse-noo/vn0125.html º °
asenda vastse-noo/vn0116.html º °

