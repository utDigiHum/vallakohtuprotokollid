#Koostab morf analüüsi käigus tundmatuks jäänud sõnadest sagedusloendi.
#Seda saab hiljem käsitsi parandades kasutajasõnastiku koostamiseks ette anda.
from __future__ import print_function
import os
import operator
import collections
import sys

freq=collections.defaultdict(int)
freq_per_parish={}
morphAnalyses=sys.argv[1]
if os.path.isdir(morphAnalyses):
	for root, dirs, files in os.walk(morphAnalyses):
		if len(files) > 0:
			parish=root.split("/")[-1]
			for file in files:
				if not file.endswith("tsv"):
					continue
				with open(os.path.join(root, file)) as fin:
					#print (i)
					for line in fin:
						word=line.split("\t")[0]
						if word not in freq_per_parish:
							freq_per_parish[word]=collections.defaultdict(int)
						if "####" in line:
							word=line.split("\t")[0]
							freq[word]+=1
							freq_per_parish[word][parish]+=1
sorted_freq = sorted(freq.items(), key=operator.itemgetter(1))
for word in reversed(sorted_freq):
	
	print ("%s\t%d\t" %(word[0], word[1]), end="")
	for key in freq_per_parish[word[0]]:
		print (key, freq_per_parish[word[0]][key], "\t", end="")
	print ()

