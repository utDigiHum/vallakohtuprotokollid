#!/usr/bin/python
#-*- coding: utf-8 -*-
import os
import sys
from collections import defaultdict
import re
#Sisendfailide kaust
inputdir=sys.argv[1]
#Väljundfailide kaust
outputdir=sys.argv[2] if len(sys.argv)>2 else "kontrollitud_failid_yhese_analyysiga"
mistakesFile=sys.argv[3] if len(sys.argv)>3 else "morf_analyysi_kontrollimise_vead.txt"
if not os.path.exists(outputdir):
	os.mkdir(outputdir)
def isNumber(word, analysis):
	romanNumerals=['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII', 'XIII', 'XIV', 'XV', 'XVI', 'XVII', 'XVIII', 'XIX', 'XX']
	if "\tO\t" in analysis or "\tN\t" in analysis:
		if re.match(r'^[0-9]', word) or word in romanNumerals:
			return True
	return False

print ("failinimi\tSõnu kokku kirjavahemärkidega\tSõnu kokku ilma kirjavahemärkideta\tÜhese analüüsiga kirjavahemärkidega\tÜhese analüüsiga ilma kirjavahemärkideta\tÜks automaatsetest analüüsidest õige\tAutomaatselt valesti ja käsitsi parandatud\tAnalüsaatori jaoks tundmatu ja käsitsi parandatud\tKäsitsi tundmatuks jäänud\tAnalüüse kokku")
with open(mistakesFile, "w") as fout:
	fout.write("#Käsitsi kontrollimise käigus märkimata jäänud mitmesed analüüsid\n")
for subdir in os.listdir(inputdir):
	if not os.path.exists(os.path.join(outputdir, subdir)):
		os.mkdir(os.path.join(outputdir, subdir))
	for filename in os.listdir(os.path.join(inputdir, subdir)):
		with open(os.path.join(inputdir, subdir, filename), encoding="utf-8") as fin:
			words=[]
			word=[]
			for line in fin:
				#Vaatame, kas rida algab tabi või tühikuga. Kui jah, siis on tegu sama sõna alternatiivse analüüsiga. Kui mitte, on tegu uue sõnaga.
				if (line.startswith("\t") or line.startswith(" ")) and word:
					line="\t"+line.lstrip()
					word.append(line)
				else:
					if len(word) != 0:
						words.append(word)
					word=[line]
			#Loendur sõnadest, millel on vaid üks analüüs (koos kirjavahemärkidega).
			singleRec=[0,0]
			#Sõnu kokku ilma kirjavahemärkideta.
			noPunctuation=[0, 0]
			
			#Sõnad, millel on mitu analüüsi ja õige neist on märgitud
			multiRec=[0, 0]
			#SÕnad, mis on analüsaatori jaoks tundmatud ja millele on analüüs käsitsi lisatud.
			manualRec=[0, 0]
			#Tundmatuks jäänud analüüs
			unknownRec=[0, 0]
			#Analüsaatori poolt valesti analüüsitud ja käsitsi korrigeeritud.
			falseRec=[0, 0]
			analysesTotal=0
			fout=open(os.path.join(outputdir, subdir, filename), "w", encoding="utf-8")
			#Kontrollimise käigus õige märgita jäänud analüüside loend.
			mistakes=[]
			#print (filename)
			for word in words:
				#print (word)
				analysesTotal+=len(word)
				#Vaatame, mitu rida analüüs sisaldab.
				if len(word) == 1:
					if "\tZ\t" not in word[0]:
						noPunctuation[1]+=1 if not(("\tO\t" in word[0] or "\tN\t" in word[0]) and "\t?\n" in word[0]) else 0
						noPunctuation[0]+=1
					singleRec[0]+=1
					singleRec[1]+=1 if not(("\tO\t" in word[0] or "\tN\t" in word[0]) and "\t?\n" in word[0]) else 0
					fout.write(word[0])
				elif len(word) > 1:
					hasCorrectAnalysis=False
					#print (word[0])
					#Analüüsitav sõnavorm, mis pärast õige analüüsi algusse lisatakse, et hiljem väljundfaili kirjutada.
					wordform=word[0].split("\t")[0]
					for analysis in word:
						#print (analysis)
						#morfoloogiline info. Kõigepealt eemaldame analüüsitava sõnavormi või tabi algusest ja siis paneme uuesti stringiks kokku.
						morf=analysis.split("\t")[1:]
						morf="\t".join(morf)
						#Kuna mõnel juhul tuleb toimida veidi teisiti kui tavapäraselt, siis ei saanud kahjuks seda mõistlikult funktsiooniga lahendada ning koodijupp tuleb pikk ja lohisev.
						if morf.startswith("@"):
							#Kontrollime, kas sõnal on õigeks märgitud  analüüs juba eelnevalt olemas. Kui jah, siis lisame selle vigade hulka.
							if hasCorrectAnalysis==True:
								mistakes.append(word)
							else:
								multiRec[0]+=1
								hasCorrectAnalysis=True
								multiRec[1]+=1 if not(("\tO\t" in morf or "\tN\t" in  morf) and "\t?\n" in morf) else 0
						elif morf.startswith("£"):
							if hasCorrectAnalysis==True:
								mistakes.append(word)
							else:
								falseRec[0]+=1
								hasCorrectAnalysis=True
								falseRec[1]+=1 if not(("\tO\t" in morf or "\tN\t" in  morf) and "\t?\n" in morf) else 0
						elif re.match("#[A-Üa-ü0-9]", morf):
							if hasCorrectAnalysis==True:
								mistakes.append(word)
							else:
								manualRec[0]+=1
								hasCorrectAnalysis=True
								manualRec[1]+=1 if not(("\tO\t" in morf or "\tN\t" in  morf) and "\t?\n" in morf) else 0
						elif morf.startswith("¤"):
							if hasCorrectAnalysis==True:
								mistakes.append(word)
							else:
								unknownRec[0]+=1
								hasCorrectAnalysis=True
								unknownRec[1]+=1 if not(("\tO\t" in morf or "\tN\t" in  morf) and "\t?\n" in morf) else 0
								morf="¤\t\t\tT\t\n"

					if hasCorrectAnalysis==False:
						mistakes.append(word)
					else:
						fout.write(wordform+"\t"+morf[1:])
			fout.close()
			#Kõigide sõnade arvu ilma kirjavahemärkideta leidmiseks.
			punctuation=singleRec[0]-noPunctuation[0]
			#singleRecNoPunc=singleRec -punctuation
			print ("%s\t%d\t%d\t%d - %d\t%d - %d\t%d - %d\t%d - %d\t%d - %d\t%d - %d\t%d" %(filename, len(words), len(words)-punctuation, singleRec[0], singleRec[1], noPunctuation[0], noPunctuation[1], multiRec[0], multiRec[1], falseRec[0], falseRec[1], manualRec[0], manualRec[1], unknownRec[0], unknownRec[1], analysesTotal), )
			with open(mistakesFile, "a") as fout:
				if len(mistakes) > 0:
					fout.write(filename+"\n")
					for mistake in mistakes:
							fout.write("".join(mistake))