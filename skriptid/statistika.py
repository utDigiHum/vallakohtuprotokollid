#!/usr/bin/python3
import os
import sys
import re
from estnltk import synthesize
from estnltk import Text
import distance
dir=sys.argv[1]
all=0
per=0
loc=0
org=0
fout=open("anomaaliad.txt", "w")
print ("*Märgendatud nimeüksusi*")
print ("Vald\tisik\tkoht\torg\tkokku")
for vald in os.listdir(dir):
	vper=0
	vloc=0
	vorg=0
	for i in os.listdir(os.path.join(dir, vald)):
		with open(os.path.join(dir, vald, i)) as fin:
			content=fin.read()
			vper+=content.count("<isik>")
			vloc+=content.count("<koht>")
			vorg+=content.count("<org>")
	vall=vper + vloc + vorg
	per+=vper
	loc+=vloc
	org+=vorg
	all+=vall
	print ("%s\t%d - %f\t%d - %f\t%d - %f\t%d" %(vald, vper, vper/vall*100, vloc, vloc/vall*100, vorg, vorg/vall*100, vall))
print ("Kokku\t%d - %f\t%d - %f\t%d - %f\t%d" %(per, per/all*100, loc, loc/all*100, org, org/all*100, all))
#Vaatame, kui palju leidub protokollis märgendamata lause keskel olevaid suure algustähega sõnu
count=0
#Lause keskel tuvastatud nimeüksuste loendur
ncount=0
anomalies={}
#Kuna päris palju on teksti keskel valetuvastusi nagu rahaühikud või kuudenimed, siis võime neid ignoreerida
falserec=["Rubla", "Rbl", "Rubl"]
#Kuna kuudenimed võivad olla käänatud, siis sünteesime kõik eri vormid ja vaatame pärast, kas tekstis mõni neist esineb. skripti jooksutamise koha pealt tundub see mõistlikum kui hakata igat nime morfoloogiliselt analüüsima.
def synthesize_all(word):
	cases = ['n', 'g', 'p', 'ill', 'in', 'el', 'all', 'ad', 'abl', 'tr', 'ter', 'es', 'ab', 'kom']
	forms=[]
	for case in cases:
		forms+=synthesize(word, "sg "+case)
	#Kuunimed suure algustähega
	for i in range(len(forms)):
		forms[i]=forms[i].capitalize()
	return forms

falserec+=synthesize_all("Jaanuar")
falserec+=synthesize_all("Veebruar")
falserec+=synthesize_all("Märts")
falserec+=synthesize_all("Aprill")
falserec+=synthesize_all("Mai")
falserec+=synthesize_all("Juuni")
#Juuli ja Augusti jätame vahele, kuna need võivad ka nimed olla
falserec+=synthesize_all("September")
falserec+=synthesize_all("Oktoober")
falserec+=synthesize_all("November")
falserec+=synthesize_all("Detsember")
#print (falserec)
print ("*Tuvastamata nimeüksused*")
fout.write("*Tuvastamata nimeüksused*\n")
print ("vald\ttuvastamata\ttuvastatud\ttuvastamata osakaal")
for vald in os.listdir(dir):
	vcount=0
	vncount=0
	for i in os.listdir(os.path.join(dir, vald)):
		anomalies[i]=[]
		with open(os.path.join(dir, vald, i)) as fin:
			content=fin.read()
			content=re.findall("<sisu>(.*?)</sisu>", content, re.DOTALL)[0]
			#print (content)
			#Loeme kokku, palju on lause keskel tuvastatud nimeüksusi sõnade kaupa
			recognised=[]
			recognised+=re.findall("[^^]<isik>[^<]*</isik>", content)
			recognised+=re.findall("[^^]<loc>[^<]*</loc>", content)
			recognised+=re.findall("[^^]<org>[^<]*</org>", content)
			for r in recognised:
				r=r.split(" ")
				vncount+=len(r)
			#Eemaldame nimeüksuste märgendid
			content=re.sub("<isik>[^<]*</isik>", "", content)
			content=re.sub("<koht>[^<]*</koht>", "", content)
			content=re.sub("<org>[^<]*</org>", "", content)
			#print (content)
			#Tükeldame teksti ridade kaupa ja vaatame, kas rea keskel on suure algustähega sõnu.
			content=content.split("\n")
			for line in content:
				line=line.strip()
				words=Text(line).word_texts
				for word in words:
					if word[0].isupper() and not line.startswith(word) and word not in falserec:
						anomalies[i].append(word)
		vcount+=len(anomalies[i])
	print ("%s\t%d\t%d\t%f" %(vald, vcount, vncount, vcount*100/(vcount+vncount)))
	
	count+=vcount
	ncount+=vncount
for p in anomalies:
	if len(anomalies[p]) > 0:
		fout.write(p+"\n")
		fout.write("\n".join(anomalies[p])+"\n")
print ("Anomaaliaid kokku\t%d\t%d\t%f" %(count, ncount, count*100/(ncount+count)))

#Vaatame, kui palju on nimeüksusi eri liigiti sassi läinud
#mixedUp={}
print("*Nimeüksused, mis on omavahel sassi läinud*")
print ("vald\tisik - koht\tisik - org\tkoht - org\sassiläinud kokku\tunikaalseid kokku")
fout.write("*Nimeüksused, mis on omavahel sassi läinud*\n")
per_loc=0
per_org=0
loc_org=0
all=0
#Unikaalseid nimesid kokku
total=0
for vald in os.listdir(dir):
	sys.stderr.write(vald+"\n")
	
	per={}
	loc={}
	org={}
	names=[]
	vtotal=[]
	for i in os.listdir(os.path.join(dir, vald)):
		with open(os.path.join(dir, vald, i)) as fin:
			content=fin.read()
			content=re.findall("<sisu>(.*?)</sisu>", content, re.DOTALL)[0]
			per[i]=re.findall("<isik>(.*?)</isik>", content, re.DOTALL)
			loc[i]=re.findall("<koht>(.*?)</koht>", content, re.DOTALL)
			org[i]=re.findall("<org>(.*?)</org>", content, re.DOTALL)			
			
			names+=per[i] + loc[i] + org[i]
	#Lisame unikaalsete nimede loendisse esimese nime, et oleks hiljem itereerides võimalik võrrelda
	vtotal.append(names[0])
	#print (len(names))
	for x in names:
		exists=False
		for y in vtotal:
			if distance.levenshtein(x, y) <= 1:
				exists=True
		if exists==False:
			vtotal.append(x)
	vtotal=len(vtotal)
	total+=vtotal
	def getMixed(x, y):
		#Anomaaliate järjend, mis hiljem faili kirjutatakse
		mixedUp=[]
		#Valla kohta anomaaliate loendamiseks
		vmixedUp=[]
		for pr1 in x:
			for a in x[pr1]:
				for pr2 in y:
					for b in y[pr2]:
						if len(a) < 4 or len(b) < 4:
							continue
						if distance.levenshtein(a, b) <= 1:
							mixedUp.append((pr1, a, pr2, b))
							vmixedUp.append((a, b))
		mixedUp=set(mixedUp)
		vmixedUp=set(vmixedUp)
		for a in mixedUp:
			fout.write(" ".join(a)+"\n")
		return len(vmixedUp)
	fout.write("*isiku- ja kohanimed*\n")
	vper_loc=getMixed(per, loc)
	fout.write("*isiku- ja organisatsiooninimed*\n")
	vper_org=getMixed(per, org)
	fout.write("*koha- ja organisatsiooninimed*\n")
	vloc_org=getMixed(loc, org)
	vall=vper_loc+vper_org+vloc_org
	per_loc+=vper_loc
	per_org+=vper_org
	loc_org+=vloc_org
	all+=vall
	print ("%s\t%d - %f\t%d - %f\t%d - %f\t%d - %f\t%d" %(vald, vper_loc, vper_loc*100/vtotal, vper_org, vper_org*100/vtotal, vloc_org, vloc_org*100/vtotal, vall, vall*100/vtotal, vtotal))
print ("Kokku\t%d - %f\t%d - %f\t%d - %f\t%d - %f\t%d" %(per_loc, per_loc*100/total, per_org, per_org*100/total, loc_org, loc_org*100/total, all, all*100/total, total))
fout.close()