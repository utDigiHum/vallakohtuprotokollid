# Vallakohtuprotokollid

Siin asuvad projekti "1860-80 vallakohtuprotokollid kultuurimälu kandjana (EKKM17-458)" avalikult kättesaadavad materjalid.
Kaustas on 6 alamkausta.

## HTML_failid
Kaustas html-symbolitega.tar.gz on algsed, serverist history.ee veebiarhiivide kaudu taastatud HTML-failid koos veebilehe elementidega. 
Kaustas html-puhastatud.tar.gz on need HTML-failid puhastatud lubamatutest sümbolitest. Lubamatute sümbolite leidmiseks on kasutatud skriptide kaustas skripti symbolid.py, sümbolite asendamiseks skripti asenda-symbolid.sh.

## XML_failid
Kaustas XML.tar.gz on HTML-kujult XML-kujule üle viidud failid. Alles on jäetud vaid failide sisuosa, omaette XML-märgenditega on eraldatud protokolli pealkiri, teema, vald, aeg, number, kohtumehed ja protokolli sisu ise. Selleks on kasutatud skripte margenda.py, AnomaaliateOtsing.py.
Kaustas xml-nimedega.tar.gz on XML-märgendusega failide sisuosas märgendatud ka isiku-, koha- ja organisatsiooninimed. Selleks on kasutatud skripti nimed.py.

## Morf_analüüsitud_failid
Kaustas morf-analyysid.tsv.zip on esialgsed automaatselt morfoloogiliselt analüüsitud failid. Failides on nii analüüsita (tundmatuid) keelendeid, mitmeseid analüüse kui ka valesid analüüse. Kasutatud on skripti morf_analyys.py.
Kaustas KontrollitudFailid.zip on käsitsi kontrollitud, parandatud ja ühestatud failid. Igast vallast on kontrollitud vähemalt 2 suuremat faili.
Kaustas morf-analyysid-kasutajasonastiku-ja-lausepiiridega.zip on morfoloogilisele analüüsile lisatud kasutajasõnastik ja lausepiirid (skript sentenceBoundaries.py).

## TSV_failid_loplik
Kaustas TSV_automaatne on kasutajasõnastikuga automaatselt morfoloogiliselt analüüsitud failid koos lausepiiridega. Kasutajasõnastiku jaoks on kasutatud skripti kasutajasonastik.py.
Kaustas TSV_kasitsi on failid, mille morfoloogiline märgendus on käsitsi üle kontrollitud, parandatud ja ühestatud. Igast vallast on kontrollitud vähemalt 2 kõige suuremat faili. Lisatud on ka lausepiirid.
Selle kausta failid on aluseks Vana Kirjakeele Korpusesse minevatele failidele.

## skriptid
Analüüsideks kasutatud skriptid.

## sisend_ja_väljundfailid
Skriptide sisend- ja väljundfaile, valdade koordinaadid, kaardid.